* Power Consumption

A little data science project around power consumption of a home with 6 humans and 2 rabbits.

** Input Data

A sensor on a Smartmeter can collect data about power consumption.  In periodic intervals it sends a
data set that gets stored in a log files.  Each data set is similar to the following example:

#+begin_src

2022-07-01_00:00:35 HM_4D1146_IEC_01 boot: off
2022-07-01_00:00:35 HM_4D1146_IEC_01 eState: E: 624856.8 P: 131
2022-07-01_00:00:35 HM_4D1146_IEC_01 energy: 624856.8
2022-07-01_00:00:35 HM_4D1146_IEC_01 energyCalc: 3980299.6
2022-07-01_00:00:35 HM_4D1146_IEC_01 power: 131

#+end_src

Each data set has one line with a metric called /energy/ that can be used to analyze the power consumption.  The rest of
this project is based on that line containing the word /energy/.


To gain familiarity with the data set it is necessary to look at the raw data, understand patterns and clean it for
further processing.

Update: https://wiki.fhem.de/wiki/HomeMatic_Type_powerMeter
- EnergyOffset gets updated each time the /energy/ metric is reset (this happens when the energy counter reaches 838860.7)


#+begin_src

grep -w energy Stromverbrauch-2022*

...
Stromverbrauch-2022-07.log:2022-07-08_22:12:38 HM_4D1146_IEC_01 energyCalc: 4072774.7
...

#+end_src

How many records do I have?

#+begin_src
grep -w energyCalc Stromverbrauch-2022* | wc -l
110146
#+end_src

Further cleansing to get only the needed input is possible using the /awk/ command.  I was interested to have one data set
with hourly information and one data set with data points for a whole day.

#+begin_src
grep -w energyCalc Stromverbrauch-2022* | awk -F: '{print $2,$5}' > input_by_hour.txt
grep -w energyCalc Stromverbrauch-2022* | awk -F: '{print substr($2,1,10),$5}' > input_by_day.txt
#+end_src

The /input_by_hour.txt/ contains records of this format "2022-07-08_22  4072847.4" while the /input_by_day.txt/ does not
contain the hour suffix within the first column: "2022-07-08  4072847.4".

With this input txt files prepared I wanted to understand how many records per day I had gathered.  With the following
/awk/ commands put into a file /records_per_day.awk/ I got a count of records per day:

#+begin_src
{sum[$1]++}
#{printf("%s : %f : %f\n", $1, sum[$1], delta) }
END {
  for (key in sum) {
    printf("%s has \t%i records\n", key, sum[key])
  }
}
#+end_src

To invoke this /awk/ script run "awk -f records_per_day.awk input_by_day.txt | sort".

** Calculating the daily or hourly power consumption

With both prepared files /input_by_day.txt/ and /input_by_hour.txt/ and another awk script it is possible to sum up the
power consumption per day resp.  per hour.  The following awk script stored in /delta_and_sum.awk/:

#+begin_src

#on first line of input we only store the value of the 2nd column
NR==1{old=$2; next}

#all other records continue with calculating the delta between previous and current value found in column $2
{delta = $2 - old}

#this "if" can be used to handle reset within the "energy" field.
#
#{if (delta <0) {
#    delta = delta + 838860.7
#  }
#}

#{printf("%s \t %.2f \t %.2f \n", $1, $2, delta)}

{old = $2}
{sum[$1] += delta}
#{printf("%s : %f : %f\n", $1, sum[$1], delta) }
END {
  for (key in sum) {
    printf("%s\t%.2f Wh\n", key, sum[key])
  }
}

#+end_src

To invoke run "awk -f delta_and_sum.awk input_by_day.txt | sort" resp.  "awk -f delta_and_sum.awk input_by_hour.txt | sort"

** Plotting the data

#+begin_src

gnuplot> set datafile separator ","
gnuplot> set xdata time
gnuplot> set timefmt "%Y-%m-%d"
gnuplot> set format x "%Y-%m-%d"
gnuplot> set format y "%.2%f\"
gnuplot> plot 'input_plot_days.csv' using 1:2 with lines

#+end_src
