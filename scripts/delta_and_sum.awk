#on first line of input we only store the value of the 2nd column
NR==1{old=$2; next}

#all other records continue with calculating the delta between previous and current value found in column $2
{delta = $2 - old}

#this "if" can be used to handle reset within the "energy" field.

{if (delta <0) {
    delta = delta + 838860.7
  }
}

#{printf("%s \t %.2f \t %.2f \n", $1, $2, delta)}

{old = $2}
{sum[$1] += delta}
#{printf("%s : %f : %f\n", $1, sum[$1], delta) }
END {
  for (key in sum) {
    printf("%s,%.2f\n", key, sum[key])
  }
}
